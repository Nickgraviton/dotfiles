"vim-plug
":PlugInstall to install listed plugins

call plug#begin()

  Plug 'scrooloose/nerdtree'
  Plug 'scrooloose/syntastic'
  Plug 'airblade/vim-gitgutter'
  Plug 'scrooloose/nerdcommenter'
  Plug 'dracula/vim', { 'as': 'dracula' }
  Plug 'ryanoasis/vim-devicons'
  Plug 'arcticicestudio/nord-vim'
  Plug 'Vimjas/vim-python-pep8-indent'
  Plug 'stephpy/vim-yaml'

call plug#end()

colorscheme nord

set termguicolors
set clipboard=unnamedplus
set tabstop=2
set shiftwidth=2
set expandtab
set number          " show line numbers
set incsearch       " search as characters are entered
set hlsearch        " highlight matches

let mapleader=","       " leader is comma
" turn off search highlight
nnoremap <leader><space> :nohlsearch<CR>
