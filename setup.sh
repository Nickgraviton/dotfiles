#!/usr/bin/env bash

# The script should be run as a user with sudo access
# It's assumed that the following packages were installed during installation:
#       base-devel, neovim, man-db, man-pages
# For networking either of the two:
# 1)    dialog, dhcpcd, netctl, wpa_supplicant
# 2)    networkmanager
#
# In both cases a systemd unit should be enabled:
# 1)    for a specific connection if using netctl
# 2)    for the network manager service itself

OS=$(cat /etc/os-release | awk -F '=' '/^PRETTY_NAME/{print $2}' | tr -d '"')

if [ "$OS" == "Arch Linux" ]
then
  # Disable pcspkr
  sudo rmmod pcspkr
  sudo bash -c 'echo "blacklist pcspkr" > /etc/modprobe.d/nobeep.conf'

  # Disable power saving for the sound card
  if lsmod | grep -q "snd_hda_intel"
  then
    sudo bash -c 'echo "options snd-hda-intel power_save=0 power_save_controller=N" > /etc/modprobe.d/alsa-base.conf'
  fi

  # Sort mirror list
  sudo pacman -S reflector
  sudo reflector --country Greece --protocol https --sort rate --save /etc/pacman.d/mirrorlist

  sudo pacman -S curl wget tree neofetch xclip python-pip git openssh htop xdg-utils bat \
      noto-fonts-emoji noto-fonts-cjk noto-fonts ttf-roboto ttf-ubuntu-font-family ttf-fira-code powerline-fonts \
      xorg-server xorg-apps lightdm lightdm-gtk-greeter lightdm-gtk-greeter-settings xfce4 xfce4-goodies \
      zathura libreoffice-still pulseaudio firefox vlc baobab pavucontrol xdg-user-dirs-gtk rofi \
      arc-gtk-theme papirus-icon-theme plank gvfs file-roller unrar p7zip ntfs-3g network-manager-applet redshift

  # Enable network manager applet, plank and redshift autostart
  mkdir -p ~/.config/autostart
  cp .config/autostart/* ~/.config/autostart/

  # Copy redshift config
  mkdir -p ~/.config/redshift
  cp .config/redshift/redshift.conf ~/.config/redshift/

  # Copy rofi config
  mkdir -p ~/.config/rofi
  cp .config/rofi/* ~/.config/rofi/

  # Enable the lightdm greeter
  sudo systemctl enable lightdm.service
  sudo cp etc/lightdm/lightdm-gtk-greeter.conf /etc/lightdm/

  # Download vim-plug
  mkdir -p ~/.config/nvim
  sh -c 'curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs \
       https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
  cp .config/nvim/init.vim ~/.config/nvim/init.vim
  nvim +PlugInstall +qall!

  # Copy bashrc
  cp .bashrc ~/.bashrc

  # Xfce terminal Dracula and Nord themes
  mkdir -p ~/.local/share/xfce4/terminal/colorschemes
  cp .local/share/xfce4/terminal/colorschemes/* ~/.local/share/xfce4/terminal/colorschemes/

  # Xfce mousepad Dracula and Nord themes
  mkdir -p ~/.local/share/gtksourceview-3.0/styles
  cp .local/share/gtksourceview-3.0/styles/* ~/.local/share/gtksourceview-3.0/styles/
  gsettings set org.xfce.mousepad.preferences.view color-scheme 'nord'

  # Xfce config files
  mkdir -p ~/.config/xfce4/xfconf/xfce-perchannel-xml/
  cp .config/xfce4/xfconf/xfce-perchannel-xml/* ~/.config/xfce4/xfconf/xfce-perchannel-xml/

  # Install yay
  git clone https://aur.archlinux.org/yay.git
  cd yay/
  makepkg -si
  cd ..
  rm -rf yay/

  yay -S nerd-fonts-fira-code starship-bin ttf-ms-fonts ventoy-bin vscodium-bin

  yay -Yc
  yes | yay -Sc

  xdg-user-dirs-update
  xdg-user-dirs-gtk-update

  # Copy genmon bash scripts
  mkdir -p ~/.local/bin
  cp .local/bin/*.sh ~/.local/bin
else
  echo "Script is intended for use in Arch linux"
fi
