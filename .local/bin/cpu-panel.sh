#!/usr/bin/env bash
# Script to be used with an xfce generic monitor plugin

cpu_warning_threshold=70
cpu_alarm_threshold=90

cpu=$(top -bn 2 -d 0.2 | grep '^%Cpu' | tail -n 1 | awk '{printf "%02d", $2+$4+$6}')

color="#A3BE8C"
if [ $cpu -gt $cpu_alarm_threshold ]
then
  color="#FF6C6B"
elif [ $cpu -gt $cpu_warning_threshold ]
then
  color="#EBCB8B"
fi
cpu="${cpu}%"

echo "<txt><span foreground="\'$color\'"> CPU:  $cpu </span></txt>"
echo "<tool>cpu: $cpu</tool>"
