#!/usr/bin/env bash
# Script to be used with an xfce generic monitor plugin

memory_warning_threshold="5"
memory_alarm_threshold="7.5"

total=$(free -b | grep Mem | awk '{$2 = $2 / 1073741824; printf "%.2fG", $2}')
used=$(free -b | grep Mem | awk '{$3 = $3 / 1073741824; printf "%.2f", $3}')
free=$(free -b | grep Mem | awk '{$4 = $4 / 1073741824; printf "%.2fG", $4}')
shared=$(free -b | grep Mem | awk '{$5 = $5 / 1073741824; printf "%.2fG", $5}')
avail=$(free -b | grep Mem | awk '{$7 = $7 / 1073741824; printf "%.2fG", $7}')

color="#81A1C1"
if (( $(echo "${used} > ${memory_alarm_threshold}" | bc -l) ))
then
  color="#FF6C6B"
elif (( $(echo "${used} > ${memory_warning_threshold}" | bc -l) ))
then
  color="#EBCB8B"
fi
used="${used} / ${total}"

echo "<txt><span foreground="\'$color\'"> MEM:  $used </span></txt>"
echo -e "<tool>mem: \t$used used\n\t\t$free free\n\t\t$shared shared\n\t\t$avail avail</tool>"
