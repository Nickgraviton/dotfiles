#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# Append the $HOME/.local/bin directory to the path if it exists
if [ -d "$HOME/.local/bin" ]
then
    PATH="$HOME/.local/bin:$PATH"
fi

# Expand the history size
export HISTFILESIZE=10000
export HISTSIZE=500
export EDITOR='nvim'
export VISUAL='nvim'
export BAT_THEME='Nord'
export MANPAGER="nvim +Man!"

alias diff='diff --width=$COLUMNS --suppress-common-lines --side-by-side --color=auto'
alias grep='grep --color=auto'
alias ls='ls --color=auto'
alias tree='tree -C'

alias cat='bat'
alias vim='nvim'
alias vi='nvim'

# Shortened ip output with color
alias ipa='ip -br -color=auto a'
alias ipr='ip -br -color=auto route'

# Enable extended pattern matching
shopt -s extglob

eval "$(starship init bash)"
